*** CheapStat App ***
 This is software for CheapStat, an open-source, do-it-yourself potentiostat for analytical and educational applications
 
 Application by Martin Holicky <martin@vestigen.com>
 
 ***Copyright (c) 2013-2014 Martin Holicky***
 
 Original Cheapstat Application and Cheapstat design by 
 Aaron A Rowe, Andrew J Bonham, Ryan J White, Michael P Zimmer, Ramsin J Yadgar, Tony M Hobza, Jim W Honea, Ilan Ben-Yaacov and Kevin W Plaxco
 
 For license see below.
 *****
 
***How to use***

1. Start the application
2. Connect to serial port
3. Hit get data
4. If some data are available, they will be processed and plotted. Image and .csv data files are output automatically to folder measured_data/

****

***Troubleshooting***

- some of measurement modes are not properly implemented in CheapStat's firmware, thus their implementation in software is useless.
Implemented measurement modes are cyclic, linear-sweep and square-wave voltammetry
- Windows - make sure you have 32-bit Java installed (64-bit doesn't work, because of Serial port library)
- Linux - try running application as root to give full access to hardware
 
***License***

  This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or any 
 later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 ****************************************************************************