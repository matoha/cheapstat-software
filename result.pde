public class Result {
  short curr_range; 
  short[] SWV_forward;
  short[] SWV_reverse;
  int[] SWV_diff;
  float[] CV_current;
  short[] ACV_mag;
  short[] ACV_phase;
  float[] LSV_current;
  short[] CA_current;
  short length;
  char[] name = new char[15];
  short type;
  short op1;
  short op2;
  short op3;
  short op4;
  short op5;
  short op6;
}

