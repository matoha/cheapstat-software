/*********************************************************************************
 
 *** Cheapstat App ***
 This is app for Cheapstat, an open-source, do-it-yourself potentiostat for analytical and educational applications
 
 App by Martin Holicky <martin.holicky@gmail.com>
 
 Copyright (c) 2013-2014 Martin Holicky
 
 Original Cheapstat Application and Cheapstat design by 
 Aaron A Rowe, Andrew J Bonham, Ryan J White, Michael P Zimmer, Ramsin J Yadgar, Tony M Hobza, Jim W Honea, Ilan Ben-Yaacov and Kevin W Plaxco
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or any 
 later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 **********************************************************************************/

/***********************
 
 Created using Processing 2.0
 
 *************************/
// GUI Library
import g4p_controls.*;

// Chart library
import org.gicentre.utils.stat.*;

// Serial port (RTXT) library
import processing.serial.*;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;
// Initialization
Serial portik;
Result result;
XYChart scatterplot;                
// Set to true for more output to console
private final static boolean debug = true;

// IV converter resistor calibration value, 10 and 50 uA range (50 uA range IV converting consists
// from two paralelly connected resistors, default 165K and 33K, respectively) [Ohms]
private final static float resistor_10 = 16500.0/1.24;
private final static float resistor_50 = 27500.0/1.24;



// Declare measurement types
public final static short SWV = 0; // Square-wave voltammetry
public final static short CV = 1; // Cyclic voltammetry
public final static short ACV = 2; // Alternating-current voltammetry
public final static short LSV = 3; // Linear sweep voltammetry
public final static short CA = 5; // Chronocoulometry


public short serial_no = 1; // Serial number for exports

short i, data;

// Chart title
String nadpis = "Cheapstat App";

// Chart subtitle
String podnadpis = "Start by connecting, and when test is complete hit Get Data!";

/*
   Function: setup
 
 This function is called on startup - initialization scripts.
 
 Parameters:
 
 nothing
 
 Returns:
 
 nothing
 
 */

public void setup() {

  size(800, 500, JAVA2D);  
  frame.setIconImage( getToolkit().getImage("data/cheapstat.png") );
  // GUI uses GUIBuilder for Processing 2

  createGUI();
  customGUI();

  label1.setFont(new Font("SansSerif", Font.PLAIN, 12));
  label3.setFont(new Font("SansSerif", Font.PLAIN, 12));
  result = new Result();
  // set up graphing
  scatterplot = new XYChart(this);
  scatterplot.showXAxis(true); 
  scatterplot.showYAxis(true); 
  scatterplot.setXAxisLabel("Voltage [mV]");
  scatterplot.setYAxisLabel("Current [uA]\n\n");
  scatterplot.setPointColour(color(0, 0, 0, 100));
  scatterplot.setPointSize(5);

  // load list of available COM ports
  dropList1.setItems(Serial.list(), 0);
}

/*
   Function: draw
 
 Called after data update to redraw the plot
 
 Parameters:
 
 nothing
 
 Returns:
 
 nothing
 
 */
public void draw() {
  background(250);
  fill(20);
  scatterplot.draw(40, 100, width-40, height-100);

  textAlign(CENTER);
  text(nadpis, 400, 70);
  text(podnadpis, 400, 90);
}

/*
   Function: graf
 
 Called by Get Data button.
 
 Parameters:
 
 nothing
 
 Returns:
 
 nothing
 
 */

public void graf() {

  short type;
  type = (short) portik.read();
  switch(type) {
  case 0: 
    SWV_data();
    break;
  case 1: 
    CV_data();
    break;
  case 2: 
    println("Not properly implemented in Cheapstat Firmware");
    break;
  case 5: 
    println("Not properly implemented in Cheapstat Firmware");
    break;
  case 3: 
    LSV_data();
    break;
  default: 
    println("Error: Invalid measurement mode");
    break;
  }
  if (debug) {
    println("Measurement mode: "+ type);
  }
}

/*
   Function: header_data
 
 Receives header data from Cheapstat about measurement mode, range etc.
 
 Parameters:
 
 short type - this should be ID number of measurement mode, as declared above
 
 Returns:
 
 nothing
 
 */
public void header_data(short type) {
  println("Getting header data");
  short i;
  while (portik.available () == 0) {
  }

  // Download result name (15 bytes)
  for (i = 0; i < 15; i++)
  {
    result.name[i] = (char) portik.read();
  }
  //if(debug){
  //  println("Measurement mode name: " + result.name);
  //}

  // Download measurement parameters op1 to op5 (op6), usually two-byte short
  result.op1 = (short) (portik.read() << 8);
  result.op1 |= portik.read();
  result.op2 = (short) (portik.read() << 8);
  result.op2 |= portik.read();
  result.op2 = (short) result.op2;
  result.op3 = (short) (portik.read() << 8);
  result.op3 |= portik.read();
  result.op3 = (short)  result.op3;
  result.op4 = (short) (portik.read() << 8);
  result.op4 |= portik.read();
  result.op5 = (short) (portik.read() << 8);
  result.op5 |= portik.read();

  if (type == ACV)
  {
    result.op6 = (short) (portik.read() << 8);
    result.op6 |= portik.read();
  }
  else if (type == CA) 
  {
    result.op6 = (short) portik.read();
  }

  // Current range (10 or 50 uA)
  result.curr_range = (byte) portik.read();

  // In the next two bytes, there will be downloaded two bytes (short) with number of data points
  result.length = (short) (portik.read() << 8);
  result.length |= portik.read();

  if (debug) {
    println("Number of measurements (data points): " +result.length);
  }
}   


/*
   Function: SWV_data
 
 Downloads data for square-wave voltammetry
 
 Parameters:
 
 nothing
 
 Returns:
 
 nothing
 
 */

public void SWV_data() {
  // download measurement parameters
  header_data(SWV);

  // Each measurement consists of forward and reverse voltage
  short[] SWV_forward = new short[result.length];
  short[] SWV_reverse = new short[result.length];

  // Dowload data, amount is specified by lenght parameter downloaded in header
  for (i = 0; i < result.length; i++) {
    data = (short) (portik.read() << 8);
    data |= portik.read();
    SWV_forward[i] = data;
    data = (short) (portik.read() << 8);
    data |= portik.read();
    SWV_reverse[i] = data;
  }
  result.SWV_forward = SWV_forward;
  result.SWV_reverse = SWV_reverse;

  // There is a "check" byte at the end, must match measurement mode ID

  if (portik.read()!=SWV)
    println("Error downloading SWV data.");
  else {
    println("Square-wave voltammetry Results Received");
    result.type = SWV;
    SWV_graph();
  }
}

/*
   Function: CV_data
 
 Downloads data for cyclic voltammetry
 
 Parameters:
 
 nothing
 
 Returns:
 
 nothing
 
 */
void CV_data() {
  // Get easurement parameters
  header_data(CV);

  float[] CV_current = new float[result.length];

  // Dowload data, amount is specified by lenght parameter downloaded in header
  for (i = 0; i < result.length; i++)
  {
    data = (short) (portik.read() << 8);
    data |= portik.read();
    CV_current[i] = (float) data;
  }
  result.CV_current = CV_current;

  // There is a "check" byte at the end, must match measurement mode ID
  if (portik.read()!=CV)
    println("Error downloading CV data.");
  else
  {
    println("Cyclic voltammetry Results Received");
    result.type = CV;
    CV_graph();
  }
}

/*
   Function: LSV_data
 
 Downloads data for linear sweep voltammetry
 
 Parameters:
 
 nothing
 
 Returns:
 
 nothing
 
 */

void LSV_data() {
  // download measurement parameters
  header_data(LSV);

  float[] LSV_current = new float[result.length];

  // Dowload data, amount is specified by lenght parameter downloaded in header
  for (i = 0; i < result.length; i++)
  {
    data = (short) (portik.read() << 8);
    data |= portik.read();
    LSV_current[i] = data;
  }
  result.LSV_current = LSV_current;

  // There is a "check" byte at the end, must match measurement mode ID
  if (portik.read()!=LSV)
    println("Error downloading LSV data.");
  else
  {
    println("Linear Sweep Voltammetry Results Received");
    result.type = LSV;
    LSV_graph();
  }
}


/*
   Function: CV_graph
 
 Called in CV_data, plots received data on the chart
 
 Parameters:
 
 nothing
 
 Returns:
 
 nothing
 
 */
public void CV_graph() {
  int a, lenght;
  float b;
  String range;
  lenght = result.CV_current.length;
  result.op2 = result.op2;
  float[] CV_voltage = new float[result.length];
  float[] CV_current = new float[result.length];
  String[] export = new String[result.length+3];
  boolean up = true;
  int pocet_na_sken = (lenght/result.op4)/2;
  b = 0;
  for (a = 0; a < lenght; a++) {
    if (b > pocet_na_sken) {
      up = false;
    }
    if (b == 0 && up == false) {
      up = true;
    }

    // voltage = min + b * step
    CV_voltage[a] = (float) result.op2+b*((3300.0*result.op5)/(4096));

    // current is calculated from standard resistor value, defined in variable resistor_10 (10 uA range) or resistor_50 (50uA range) 
    if (result.curr_range == 1) {
      CV_current[a] = (((result.CV_current[a]/2048)*1.600)/resistor_10)*1000000;
    }
    else {
      CV_current[a] = (((result.CV_current[a]/2048)*1.600)/resistor_50)*1000000;
    }
    export[a+3] = CV_voltage[a] + "," + CV_current[a];
    if (up) {
      b++;
    }
    else {
      b--;
    }
  }
  nadpis = measurement_name.getText();

  if (result.curr_range == 1)
    range = "0-10 uA";
  else
    range = "0-50 uA";

  // subtitle with information about the scan  
  podnadpis = "Cyclic voltammetry " + String.format("%d to %dmV at %dmV/s %d time(s) %dmV/sample %s", result.op2, result.op3, result.op1, result.op4, result.op5, range);

  // note at the beggining of log file
  export[0] = "applied voltage, current";
  export[1] = "mV, uA";
  export[2] = nadpis +" " + podnadpis + "\n";

  // save the log
  saveStrings("measured_data/CV_" +serial_no + "_" + nadpis+"_" + day()+ month() + year() + "_"+ hour() + minute() +".csv", export);
  save("measured_data/CV_" +serial_no + "_" + nadpis+"_" + day()+ month() + year() + "_"+ hour() + minute() + ".png");
  serial_no++;
  // This also redraws the plot
  scatterplot.setData(CV_voltage, CV_current);
  // redraw titles and plot
  draw();
}

public void SWV_graph() {
  int a, b;
  String range;
  float voltage;
  float[] SWV_voltage = new float[result.length*3];
  float[] SWV_current = new float[result.length*3];
  String[] export = new String[result.length+1];
  b = 0;

  for (a = 0; a < result.length; a++) {
    voltage = (float) result.op2+a*result.op5;

    if (result.curr_range == 1) {
      SWV_voltage[b] = voltage;
      SWV_current[b] = (((result.SWV_forward[a]/2048.0)*1.600)/resistor_10)*1000000.0;
      b++;
      SWV_voltage[b] = voltage;
      SWV_current[b] = (((result.SWV_reverse[a]/2048.0)*1.600)/resistor_10)*1000000.0;
      b++;
      SWV_voltage[b] = voltage;
      SWV_current[b] = ((((result.SWV_forward[a]-result.SWV_reverse[a])/2048.0)*1.600)/resistor_10)*1000000.0;
      b++;
    }
    else {
      SWV_voltage[b] = voltage;
      SWV_current[b] = (((result.SWV_forward[a]/2048.0)*1.600)/resistor_50)*1000000.0;
      b++;
      SWV_voltage[b] = voltage;
      SWV_current[b] = (((result.SWV_reverse[a]/2048.0)*1.600)/resistor_50)*1000000.0;
      b++;
      SWV_voltage[b] = voltage;
      SWV_current[b] = ((((result.SWV_forward[a]-result.SWV_reverse[a])/2048.0)*1.600)/resistor_50)*1000000.0;
      b++;
    }
    export[a+1] = voltage + "," + (((result.SWV_forward[a]/2048.0)*1.600)/resistor_10)*1000000.0 + "," + (((result.SWV_reverse[a]/2048.0)*1.600)/resistor_10)*1000000.0;
  }

  // This also redraws the plot
  scatterplot.setData(SWV_voltage, SWV_current);

  // Set title and subtitle
  nadpis = measurement_name.getText();
  if (nadpis == "") {
    nadpis = "Square-wave Voltammetry";
  }
  if (result.curr_range == 1)
    range = "0-10 uA";
  else
    range = "0-50 uA";

  podnadpis = "Square-wave voltammetry " + String.format("%dHz, %d to %dmV by %dmV,%dmV,%s", result.op1, result.op2, result.op3, result.op4, result.op5, range);
  export[0] = "// "+nadpis +" " + podnadpis;
  // save the log
  saveStrings("measured_data/SWV_" +serial_no + "_" + nadpis+"_" + day()+ month() + year() + "_"+ hour() + minute() +".csv", export);
  save("measured_data/SWV_" +serial_no + "_" + nadpis+"_" + day()+ month() + year() + "_"+ hour() + minute() + ".png");
  serial_no++;    
  // redraw titles and plot
  draw();
}

public void LSV_graph() {
  int a, lenght, min, step;
  float b;
  String range;
  lenght = result.LSV_current.length;
  float[] LSV_voltage = new float[result.length];
  float[] LSV_current = new float[result.length];
  String[] export = new String[result.length+1];
  min = result.op2;
  step = result.op5;
  b = step;
  for (a = 0; a < lenght; a++) {
    // X axis calculations
    LSV_voltage[a] = (float) min+b*a;
    LSV_current[a] = (((result.LSV_current[a]/2048.0)*1.600)/resistor_10)*1000000;
  }

  // This also redraws the plot
  scatterplot.setData(LSV_voltage, LSV_current);

  // Set title and subtitle
  nadpis = measurement_name.getText();
  if (nadpis == "") {
    nadpis = "Linear-sweep Voltammetry";
  }
  if (result.curr_range == 1)
    range = "0-10 uA";
  else
    range = "0-50 uA";

  podnadpis = "Linear-Sweep Voltammetry " + String.format("%dmV to %dmV \nat %dmV/s, %dmV/sample, %s", result.op2, result.op3, result.op4, result.op5, range);
  export[0] = "// "+nadpis +" " + podnadpis;
  saveStrings("measured_data/LSV_" +serial_no + "_" + nadpis+"_" + day()+ month() + year() + "_"+ hour() + minute() +".csv", export);
  save("measured_data/LSV_" +serial_no + "_" + nadpis+"_" + day()+ month() + year() + "_"+ hour() + minute() + ".png");
  
  serial_no++;    

  // redraw titles and plot
  draw();
}

public void customGUI() {
}

